# Your code goes here.
import json
from datetime import datetime


def open_file(filename: str) -> list:
    """
    Open a json file and return its content as a list
    """
    with open(filename, 'r') as f:
        json_content = f.read()
        film_list = json.loads(json_content)
    return film_list


def is_valid(key: str, my_dict: dict) -> bool:
    """
    Verify if the given dict key exists and defined
    """
    return key in my_dict and my_dict[key] != 'N/A'


def count(filename: str) -> int:
    """
    Open a json file and count its elements
    """
    film_list = open_file(filename)
    res = len(film_list)
    return res


def written_by(filename: str, author: str) -> list:
    """
    Open a json file and search for the films written by the given author
    """
    film_list = open_file(filename)
    res = [x for x in film_list if author in x['Writer']]
    return res


def longest_title(filename: str) -> dict:
    """
    Open a json file and search for the film with the longest title
    """
    film_list = open_file(filename)
    res = sorted(film_list, key=lambda x: len(x['Title']), reverse=True)[0]
    return res


def best_rating(filename: str) -> dict:
    """
    Open a json file and search for the best rated film
    """
    film_list = open_file(filename)
    film_with_rating = [x for x in film_list if is_valid('imdbRating', x)]
    res = sorted(film_with_rating, key=lambda x: x['imdbRating'], reverse=True)[0]
    return res


def latest_film(filename: str) -> str:
    """
    Open a json file and search for the latest film title
    """
    film_list = open_file(filename)
    film_with_date = [x for x in film_list if is_valid('Released', x)]
    res = sorted(film_with_date, key=lambda x: datetime.strptime(x['Released'], '%d %b %Y'), reverse=True)[0]
    return res['Title']


def find_per_genre(filename: str, genre: str) -> list:
    """
    Open a json file and search for films with the given genre
    """
    film_list = open_file(filename)
    film_with_genre = [x for x in film_list if is_valid('Genre', x)]
    res = [x for x in film_with_genre if genre in x['Genre']]
    return res


def released_after(filename: str, date) -> list:
    """
    Open a json file and search for the films released after a given date in format dd/mm/yyyy
    """
    film_list = open_file(filename)
    film_with_date = [x for x in film_list if is_valid('Released', x)]
    res = [x for x in film_with_date if datetime.strptime(x['Released'], '%d %b %Y') >= datetime.strptime(date, '%d/%m/%Y')]
    return res
